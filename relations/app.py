import views
from config import app

relations_view = views.RelationsAPIVIew.as_view('relations')
app.add_url_rule('/relations', view_func=relations_view, methods=['POST', 'GET'])
app.add_url_rule('/relations/<user_id>', view_func=relations_view,
                 methods=['GET', 'DELETE'])

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
