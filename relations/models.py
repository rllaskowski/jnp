from bson.objectid import ObjectId
from config import db


class Relations(db.Document):
    id = db.ObjectIdField(default=ObjectId, primary_key=True)
    followed_users_ids = db.ListField(field=db.ObjectIdField())
    followed_games_ids = db.ListField(field=db.ObjectIdField())

    def __repr__(self):
        return f'{self.user_id}'
