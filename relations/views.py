from bson.objectid import ObjectId
from flask import request, abort, make_response
from flask.views import MethodView
from marshmallow import Schema, fields, ValidationError, validates_schema
from config import app, SERVICE_NAME
from flask_jwt_extended import jwt_required, get_jwt_identity
from models import Relations


def validate_mongo_id(x):
    if len(x) != 12 and len(x) != 24:
        raise ValidationError('Incorrect mongo id.')


def abort_on_incorrect_mongo_id(mongo_id):
    try:
        validate_mongo_id(mongo_id)
    except ValidationError as err:
        abort(make_response({'id': err.messages}, 422))


def load_json(schema, partial=False):
    json_data = request.get_json()
    if not json_data:
        abort(make_response({'errors': ['No input data provided.']}, 400))
    try:
        data = schema.load(json_data, partial=partial)
    except ValidationError as err:
        abort(make_response(err.messages, 422))

    return data


class RelationsSchema(Schema):
    id = fields.String(required=True, validate=validate_mongo_id)
    followed_users_ids = fields.List(fields.String(), dump_only=True)
    followed_games_ids = fields.List(fields.String(), dump_only=True)


class RelationsChangesSchema(Schema):
    user_id = fields.String(load_only=True, validate=validate_mongo_id)
    game_id = fields.String(load_only=True, validate=validate_mongo_id)

    @validates_schema
    def validate_data(self, data, **kwargs):
        if 'user_id' in data and 'game_id' in data:
            raise ValidationError('Only one of ids must be provided.')


relations_schema = RelationsSchema()
relations_changes_schema = RelationsChangesSchema()


class RelationsAPIVIew(MethodView):

    @jwt_required()
    def get(self):
        user_id = get_jwt_identity()
        abort_on_incorrect_mongo_id(user_id)
        user = Relations.objects(pk=user_id)\
            .first_or_404(message='Resource not found.')

        return relations_schema.dump(user)

    def post(self):
        data = load_json(relations_schema)
        relations = Relations(id=data['id'])
        relations.save()

        return relations_schema.dump(relations), 201

    def delete(self, user_id):
        abort_on_incorrect_mongo_id(user_id)
        user = Relations.objects(pk=user_id) \
            .first_or_404(message='Resource not found.')
        user.delete()

        return '', 204


@app.route('/relations/is_followed', methods=['GET'])
@jwt_required()
def is_followed():
    user_id = get_jwt_identity()

    if 'user_id' in request.args:
        followed = Relations.objects(
            id=user_id, followed_users_ids=request.args['user_id']
        ).first() is not None
    elif 'game_id' in request.args:
        followed = Relations.objects(
            id=user_id, followed_games_ids=request.args['game_id']
        ).first() is not None
    else:
        abort(make_response("Provide user_id or game_id", 400))

    return {'is_followed': followed}


@app.route('/relations/add_relation', methods=['PATCH'])
@jwt_required()
def add_relation():
    user_id = get_jwt_identity()
    abort_on_incorrect_mongo_id(user_id)
    relations = Relations.objects(pk=user_id) \
        .first_or_404(message='Resource not found.')
    data = load_json(relations_changes_schema)

    if 'game_id' in data:
        if ObjectId(data['game_id']) not in relations.followed_games_ids:
            relations.followed_games_ids.append(data['game_id'])
        else:
            abort(make_response({'game_id': ['Given id already in relation.']}))
    else:
        if ObjectId(data['user_id']) not in relations.followed_users_ids:
            relations.followed_users_ids.append(data['user_id'])
        else:
            abort(make_response({'user_id': ['Given id already in relation.']}))

    relations.save()

    return relations_schema.dump(relations)


@app.route('/relations/remove_relation', methods=['PATCH'])
@jwt_required()
def remove_relations():
    user_id = get_jwt_identity()
    abort_on_incorrect_mongo_id(user_id)
    relations = Relations.objects(pk=user_id) \
        .first_or_404(message='Resource not found.')
    data = load_json(relations_changes_schema)

    if 'game_id' in data:
        relations.update(pull__followed_games_ids=data['game_id'])
    else:
        relations.update(pull__followed_users_ids=data['user_id'])

    relations.save()
    relations.reload()

    return relations_schema.dump(relations)


@app.route('/')
def healthcheck():
    return f'Service {SERVICE_NAME}: OK'
