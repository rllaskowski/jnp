#!/bin/bash

# "Resolve" BACKEND env variable in varnish default.vcl.
sed -i "s/\${BACKEND}/${BACKEND}/g" /etc/varnish/default.vcl

service varnish start
tail -f /dev/null
