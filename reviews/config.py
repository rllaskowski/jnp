import logging
from flask import Flask
from flask_mongoengine import MongoEngine
from pymongo import MongoClient
from pymongo.errors import OperationFailure
from flask_cors import CORS
from flask_jwt_extended import JWTManager
import os

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

SERVICE_NAME = 'reviews'

class Config:
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY')
    MONGODB_SETTINGS = {
        'host': f'mongodb://{SERVICE_NAME}_mongo1:27017,{SERVICE_NAME}_mongo2:27017/'
                f'database?replicaSet=rs0',
    }


class ProductionConfig(Config):
    DEBUG = False


class DebugConfig(Config):
    DEBUG = True
    TESTING = True


def init_replica_set():
    c = MongoClient(f'{SERVICE_NAME}_mongo1', 27017)
    config = {'_id': 'rs0', 'members': [
        {'_id': 0, 'host': f'{SERVICE_NAME}_mongo1:27017'},
        {'_id': 1, 'host': f'{SERVICE_NAME}_mongo2:27017'}
    ]}
    try:
        c.admin.command("replSetInitiate", config)
        logger.info('Replica set initialization successful.')
    except OperationFailure:
        logger.error('Replica set already initialized.')


def create_all():
    app = Flask(__name__, instance_relative_config=True)
    CORS(app)
    app.config.from_object(DebugConfig())
    db = MongoEngine(app)
    init_replica_set()
    return app, db


app, db = create_all()

jwt = JWTManager(app)


@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    return jwt_data["sub"]
