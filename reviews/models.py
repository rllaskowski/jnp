from datetime import datetime
from config import db


class Review(db.Document):
    user_id = db.StringField(required=True)
    game_id = db.StringField(required=True)
    title = db.StringField(required=True)
    description = db.StringField(required=True)
    score = db.IntField(required=True)
    image = db.ImageField()  # TODO add constraint png or jpeg
    creation_time = db.DateTimeField(default=datetime.now)
    modification_time = db.DateTimeField(default=datetime.now)

    def __repr__(self):
        return f'{self.title} {self.score} {self.user_id} {self.game_id} ' \
               f'{self.creation_time} {self.modification_time}'

    def save(self, *args, **kwargs):
        self.modification_time = datetime.now()
        return super().save(*args, **kwargs)

    def modify(self, **update):
        return super().modify(modification_time=datetime.now(), **update)
