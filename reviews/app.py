import views
from config import app

auth_reviews_view = views.AuthenticatedReviewAPIVIew.as_view('auth_reviews')
reviews_view = views.ReviewAPIVIew.as_view('reviews')
reviews_list_view = views.ReviewListAPIVIew.as_view('reviews_list')

app.add_url_rule('/reviews/authenticated', view_func=auth_reviews_view, methods=['POST'])
app.add_url_rule('/reviews/list', view_func=reviews_list_view, methods=['GET'])
reviews_image_view = views.ReviewImageAPIVIew.as_view('image')

app.add_url_rule('/reviews', view_func=reviews_view, methods=['POST'])
app.add_url_rule('/reviews/<review_id>', view_func=reviews_view,
                 methods=['GET', 'PATCH', 'DELETE'])
app.add_url_rule('/reviews/<review_id>/image', view_func=reviews_image_view,
                 methods=['GET', 'POST'])

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
