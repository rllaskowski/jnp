from flask import request, abort, make_response, send_file
from flask.views import MethodView
from marshmallow import Schema, fields, ValidationError, validate
from mongoengine import Q
from config import app, SERVICE_NAME
from models import Review
from flask_jwt_extended import jwt_required, get_jwt_identity


def validate_mongo_id(x):
    if len(x) != 12 and len(x) != 24:
        raise ValidationError('Incorrect mongo id.')


def abort_on_incorrect_mongo_id(mongo_id):
    try:
        validate_mongo_id(mongo_id)
    except ValidationError as err:
        abort(make_response({'id': err.messages}, 422))


def load_json(schema, partial=False):
    json_data = request.get_json()
    if not json_data:
        abort(make_response({'errors': ['No input data provided.']}, 400))
    try:
        data = schema.load(json_data, partial=partial)
    except ValidationError as err:
        abort(make_response(err.messages, 422))
    return data


class AuthenticatedReviewSchema(Schema):
    id = fields.String(dump_only=True)
    game_id = fields.String(required=True, validate=validate_mongo_id)
    title = fields.String(required=True)
    description = fields.String(required=True)
    score = fields.Integer(required=True, validate=validate.Range(min=0, max=10))
    creation_time = fields.DateTime(dump_only=True)
    modification_time = fields.DateTime(dump_only=True)


class ReviewSchema(AuthenticatedReviewSchema):
    user_id = fields.String(required=True, validate=validate_mongo_id)


class AuthenticatedReviewAPIVIew(MethodView):
    def __init__(self):
        self.schema = AuthenticatedReviewSchema()

    @jwt_required()
    def post(self):
        data = load_json(self.schema)
        data['user_id'] = get_jwt_identity()
        review = Review(**data)
        review.save()
        return self.schema.dump(review), 201


class ReviewAPIVIew(MethodView):
    def __init__(self):
        self.schema = ReviewSchema()

    def get(self, review_id):
        abort_on_incorrect_mongo_id(review_id)
        review = Review.objects(pk=review_id)\
            .first_or_404(message='Resource not found.')
        return self.schema.dump(review)

    def post(self):
        data = load_json(self.schema)
        review = Review(**data)
        review.save()
        return self.schema.dump(review), 201

    def patch(self, review_id):
        abort_on_incorrect_mongo_id(review_id)
        review = Review.objects(pk=review_id) \
            .first_or_404(message='Resource not found.')
        data = load_json(self.schema, partial=True)
        review.modify(**data)
        review.reload()
        return self.schema.dump(review)

    def delete(self, review_id):
        abort_on_incorrect_mongo_id(review_id)
        review = Review.objects(pk=review_id) \
            .first_or_404(message='Resource not found.')
        if review.image:
            review.image.delete()
        review.delete()
        return '', 204


class ReviewImageAPIVIew(MethodView):

    def get(self, review_id):
        abort_on_incorrect_mongo_id(review_id)
        review = Review.objects(pk=review_id) \
            .first_or_404(message='Resource not found.')
        if not review.image:
            abort(make_response({'errors': ['Image not found.']}, 400))
        return send_file(
            path_or_file=review.image,
            mimetype=review.image.content_type
        )

    def post(self, review_id):
        abort_on_incorrect_mongo_id(review_id)
        if 'image' in request.files:
            review = Review.objects(pk=review_id) \
                .first_or_404(message='Resource not found.')
            image = request.files['image']
            if not review.image:
                review.image.put(image, content_type=image.content_type)
            else:
                review.image.replace(image, content_type=image.content_type)
            review.save()
        else:
            abort(make_response({'errors': ['No image provided.']}, 400))
        return '', 204


class ReviewListAPIVIew(MethodView):
    def __init__(self):
        self.schema = ReviewSchema()

    def get(self):
        query = None
        query_params = request.args.to_dict(flat=False)

        if 'users_ids[]' in request.args:
            query = Q(user_id__in=query_params['users_ids[]'])

        if 'games_ids[]' in request.args:
            query_games = Q(game_id__in=query_params['games_ids[]'])
            query = (query | query_games) if query else query_games

        if query:
            review_list = Review.objects(query).order_by('-creation_time')
        else:
            review_list = Review.objects(**request.args).order_by('-creation_time')

        return {
            'review_list': self.schema.dump(review_list, many=True)
        }


@app.route('/')
def healthcheck():
    return f'Service {SERVICE_NAME}: OK'
