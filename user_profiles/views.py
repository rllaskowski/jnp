from flask import request, abort, make_response, Response
from flask.views import MethodView
from marshmallow import Schema, fields, ValidationError
from flask_jwt_extended import jwt_required, get_jwt_identity

from config import app, cache, SERVICE_NAME
from models import UserProfile


def validate_mongo_id(x):
    if len(x) != 12 and len(x) != 24:
        raise ValidationError('Incorrect mongo id.')


def abort_on_incorrect_mongo_id(mongo_id):
    try:
        validate_mongo_id(mongo_id)
    except ValidationError as err:
        abort(make_response({'id': err.messages}, 422))


def load_json(schema, partial=False):
    json_data = request.get_json()
    if not json_data:
        abort(make_response({'errors': ['No input data provided.']}, 400))
    try:
        data = schema.load(json_data, partial=partial)
    except ValidationError as err:
        abort(make_response(err.messages, 422))
    return data


class AuthenticatedUserProfileSchema(Schema):
    id = fields.String(dump_only=True)
    name = fields.String(required=True)
    surname = fields.String()
    description = fields.String()
    birth_date = fields.Date()
    creation_time = fields.DateTime(dump_only=True)
    modification_time = fields.DateTime(dump_only=True)


class UserProfileSchema(AuthenticatedUserProfileSchema):
    id = fields.String(required=True, validate=validate_mongo_id)


class UserProfileListAPIVIew(MethodView):
    def __init__(self):
        self.schema = UserProfileSchema()

    def get(self):
        query_params = request.args.to_dict()

        count = query_params.get('count')
        offset = query_params.get('offset', 0)

        if count:
            user_profile_list = UserProfile.objects(**request.args)[offset:count]
        else:
            user_profile_list = UserProfile.objects(**request.args)[offset:]

        return {
            'user_profile_list': self.schema.dump(user_profile_list, many=True)
        }


class UserProfileAPIVIew(MethodView):
    def __init__(self):
        self.schema = UserProfileSchema()

    @cache.cached()
    def get(self, user_id):
        abort_on_incorrect_mongo_id(user_id)
        user = UserProfile.objects(pk=user_id)\
            .first_or_404(message='Resource not found.')
        return self.schema.dump(user)

    def post(self):
        data = load_json(self.schema)
        user = UserProfile(**data)
        user.save()
        return self.schema.dump(user), 201

    def patch(self, user_id):
        abort_on_incorrect_mongo_id(user_id)
        user = UserProfile.objects(pk=user_id) \
            .first_or_404(message='Resource not found.')
        data = load_json(self.schema, partial=True)
        data.pop('id', None)
        user.modify(**data)
        user.reload()
        return self.schema.dump(user)

    def delete(self, user_id):
        abort_on_incorrect_mongo_id(user_id)
        user = UserProfile.objects(pk=user_id) \
            .first_or_404(message='Resource not found.')
        if user.image:
            user.image.delete()
        user.delete()
        return '', 204


class AuthenticatedUserProfileAPIVIew(MethodView):

    def __init__(self):
        self.schema = AuthenticatedUserProfileSchema()

    @jwt_required()
    def get(self):
        user_id = get_jwt_identity()
        abort_on_incorrect_mongo_id(user_id)
        user = UserProfile.objects(pk=user_id)\
            .first_or_404(message='Resource not found.')
        return self.schema.dump(user)

    @jwt_required()
    def patch(self):
        user_id = get_jwt_identity()
        abort_on_incorrect_mongo_id(user_id)
        user = UserProfile.objects(pk=user_id) \
            .first_or_404(message='Resource not found.')
        data = load_json(self.schema, partial=True)
        data.pop('id', None)
        user.modify(**data)
        user.reload()

        return self.schema.dump(user)


class AuthenticatedUserProfileImageAPIVIew(MethodView):

    @jwt_required()
    def post(self):
        user_id = get_jwt_identity()
        abort_on_incorrect_mongo_id(user_id)

        if 'image' in request.files:
            user = UserProfile.objects(pk=user_id) \
                .first_or_404(message='Resource not found.')
            image = request.files['image']
            if not user.image:
                user.image.put(image, content_type=image.content_type)
            else:
                user.image.replace(image, content_type=image.content_type)
            user.save()
        else:
            abort(make_response({'errors': ['No image provided.']}, 400))
        return '', 204


class UserProfileImageAPIVIew(MethodView):
    @cache.cached()
    def get(self, user_id):
        abort_on_incorrect_mongo_id(user_id)
        user = UserProfile.objects(pk=user_id)\
            .first_or_404(message='Resource not found.')
        if not user.image:
            abort(make_response({'errors': ['Image not found.']}, 400))
        return Response(response=user.image.read(), content_type=user.image.content_type)

    def post(self, user_id):
        abort_on_incorrect_mongo_id(user_id)
        if 'image' in request.files:
            user = UserProfile.objects(pk=user_id) \
                .first_or_404(message='Resource not found.')
            image = request.files['image']
            if not user.image:
                user.image.put(image, content_type=image.content_type)
            else:
                user.image.replace(image, content_type=image.content_type)
            user.save()
        else:
            abort(make_response({'errors': ['No image provided.']}, 400))
        return '', 204


@cache.cached()
def thumbnail(user_id):
    abort_on_incorrect_mongo_id(user_id)
    user = UserProfile.objects(pk=user_id) \
        .first_or_404(message='Resource not found.')
    if not user.image:
        abort(make_response({'errors': ['Image not found.']}, 400))
    return Response(response=user.image.thumbnail.read(),
                    content_type=user.image.content_type)


@app.route('/')
def healthcheck():
    return f'Service {SERVICE_NAME}: OK'
