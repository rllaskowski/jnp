from datetime import datetime
from bson.objectid import ObjectId
from config import db


class UserProfile(db.Document):
    id = db.ObjectIdField(default=ObjectId, primary_key=True)
    name = db.StringField(required=True)
    surname = db.StringField()
    image = db.ImageField(thumbnail_size=(48, 48, True))  # TODO add constraint png or jpeg
    description = db.StringField()
    birth_date = db.DateField()
    creation_time = db.DateTimeField(default=datetime.now)
    modification_time = db.DateTimeField(default=datetime.now)

    def __repr__(self):
        return f'{self.name} {self.surname}'

    def save(self, *args, **kwargs):
        self.modification_time = datetime.now()
        return super().save(*args, **kwargs)

    def modify(self, **update):
        return super().modify(modification_time=datetime.now(), **update)
