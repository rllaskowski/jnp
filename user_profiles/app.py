import views
from config import app

auth_user_profiles_view = views.AuthenticatedUserProfileAPIVIew.as_view('auth_user_profiles')
auth_user_profiles_image_view = views.AuthenticatedUserProfileImageAPIVIew.as_view('auth_user_profiles_image_view')
user_profiles_view = views.UserProfileAPIVIew.as_view('user_profiles')
user_profiles_list_view = views.UserProfileListAPIVIew.as_view('user_profiles_list')
user_profiles_image_view = views.UserProfileImageAPIVIew.as_view('image')


app.add_url_rule('/user_profiles/authenticated', view_func=auth_user_profiles_view, methods=['GET', 'PATCH'])
app.add_url_rule('/user_profiles/authenticated/image',
                 view_func=auth_user_profiles_image_view, methods=['POST'])
app.add_url_rule('/user_profiles/list', view_func=user_profiles_list_view, methods=['GET'])
app.add_url_rule('/user_profiles', view_func=user_profiles_view, methods=['POST'])
app.add_url_rule('/user_profiles/<user_id>', view_func=user_profiles_view,
                 methods=['GET', 'PATCH', 'DELETE'])
app.add_url_rule('/user_profiles/<user_id>/image', view_func=user_profiles_image_view,
                 methods=['GET', 'POST'])
app.add_url_rule('/user_profiles/<user_id>/thumbnail', view_func=views.thumbnail,
                 methods=['GET'])

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
