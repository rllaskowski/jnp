import os
import logging
from flask import Flask
from flask_mongoengine import MongoEngine
from pymongo import MongoClient
from pymongo.errors import OperationFailure
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_caching import Cache


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

SERVICE_NAME = 'user_profiles'


class Config:
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY')
    MONGODB_SETTINGS = {
        'host': f'mongodb://{SERVICE_NAME}_mongo1:27017,{SERVICE_NAME}_mongo2:27017/'
                f'database?replicaSet=rs0',
    }
    CACHE_TYPE = 'RedisCache'
    CACHE_REDIS_HOST = f'{SERVICE_NAME}_redis'
    CACHE_REDIS_PORT = 6379
    CACHE_REDIS_DB = 0
    CACHE_REDIS_URL = f'redis://{SERVICE_NAME}_redis:6379/0'
    CACHE_DEFAULT_TIMEOUT = 30


class ProductionConfig(Config):
    DEBUG = False


class DebugConfig(Config):
    DEBUG = True
    TESTING = True


def init_replica_set():
    c = MongoClient(f'{SERVICE_NAME}_mongo1', 27017)
    config = {'_id': 'rs0', 'members': [
        {'_id': 0, 'host': f'{SERVICE_NAME}_mongo1:27017'},
        {'_id': 1, 'host': f'{SERVICE_NAME}_mongo2:27017'}
    ]}
    try:
        c.admin.command("replSetInitiate", config)
        logger.info('Replica set initialization successful.')
    except OperationFailure:
        logger.error('Replica set already initialized.')


def create_all():
    app = Flask(__name__, instance_relative_config=True)
    CORS(app)
    app.config.from_object(DebugConfig())
    db = MongoEngine(app)
    cache = Cache(app)
    init_replica_set()
    return app, db, cache


app, db, cache = create_all()

jwt = JWTManager(app)


@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    return jwt_data["sub"]
