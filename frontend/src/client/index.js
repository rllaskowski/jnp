import axios from "axios";
import Cache from "./cache";


class ApiClient {
    constructor(hostUri) {
        this.instance = axios.create();
        this.hostUri = hostUri;
        this.cache = new Cache();

        this.instance.interceptors.request.use((config) => {
            if (!config.headers) {
                config.headers = {
                    "Content-Type": "application/json",
                }
            }

            const token = this.tokenProvider && this.tokenProvider.get();
            if (token) {
                config.headers["Authorization"] = `Bearer ${token}`;
            }

            return config;
        },(error) => {
            return Promise.reject(error);
        });

        this.instance.interceptors.response.use(null, (error) => {
            if (error.config && error.response && error.response.status === 401 && this.tokenProvider) {
                return this.tokenProvider.refresh().then(() => {
                    error.config.headers["x-access-token"] = `Bearer ${this.tokenProvider.get()}`;
                    return axios.request(error.config);
                })
            } else {
                return Promise.reject(error);
            }
        });
    }

    setTokenProvider(tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    get(path, params = {}, useCache = false, cacheTtl = undefined, config={}) {
        const cacheValue = useCache && this.cache.get({
            path: path,
            params: params
        });

        if (cacheValue) {
            return new Promise.resolve(cacheValue);
        }

        return this.instance.get(`${this.hostUri}${path}`, {
            params: params
        }).then((response) => {
            this.cache.set({
                path: path,
                params: params
            }, response.data, cacheTtl);

            return response.data;
        });
    }

    post(path, data = {}, config={}) {
        return this.instance.post(`${this.hostUri}${path}`, data)
            .then(response => {
                return response.data;
            });
    }

    patch(path, data = {}, config={}) {
        return this.instance.patch(`${this.hostUri}${path}`, data, config)
            .then(response => {
                return response.data;
            });
    }

}

export default ApiClient;