import axios from "axios";
import {AUTH_HOST} from "../config";


const AUTHENTICATE_URL = `${AUTH_HOST}/login`;
const REFRESH_TOKEN_URL = `${AUTH_HOST}/refresh_token`;


class TokenProvider {
    constructor (accessStorageKey, refreshStorageKey) {
        this.accessStorageKey = accessStorageKey;
        this.refreshStorageKey = refreshStorageKey;
        this.refreshRequest = null;
        this.client = axios.create();
        this.regain();
    }

    removeTokens() {
        localStorage.removeItem(this.accessStorageKey);
        localStorage.removeItem(this.refreshStorageKey);
    }

    regain() {
        this.accessToken = localStorage.getItem(this.accessStorageKey) || "";
        this.refreshToken = localStorage.getItem(this.refreshStorageKey) || "";

    }

    get() {
        return this.accessToken;
    }

    setRefresh(refreshToken) {
        this.refreshToken = refreshToken;
        localStorage.setItem(this.refreshStorageKey, this.refreshToken);
    }

    setAccess(accessToken) {
        this.accessToken = accessToken;
        localStorage.setItem(this.accessStorageKey, this.accessToken);
    }

    authenticate(email, password) {
        const data = {
            email,
            password
        };

        return this.client.post(AUTHENTICATE_URL, data)
            .then((response) => {
                const data = response.data;
                this.setAccess(data["access_token"]);
                this.setRefresh(data["refresh_token"]);
            })
    }

    refresh() {
        const data = {refresh: this.refreshToken};

        if (!this.refreshRequest) {
            this.refreshRequest = this.client.post(REFRESH_TOKEN_URL, data)
                .then((response) => {
                    const data = response.data;
                    this.setAccess(data["access_token"]);
                    this.refreshRequest = null;

                    return this.accessToken;
                });
        }

        return this.refreshRequest;
    }
}

export default TokenProvider;