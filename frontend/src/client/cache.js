import {DEFAULT_CACHE_TTL} from "../config";

class Cache {
    constructor(ttl=DEFAULT_CACHE_TTL) {
        this.cached = {};
        this.ttl = ttl;
    }

    set(key, value, ttl=undefined) {
        this.cached[key] = value;

        setTimeout(() => {
            delete this.cached[key];
        }, ttl === undefined? this.ttl : ttl);
    }

    get(key) {
        return this.cached[key];
    }
}


export default Cache;