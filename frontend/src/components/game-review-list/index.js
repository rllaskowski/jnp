import React from "react";
import GameReviewCard from "../game-review-card";


const GameReviewList = ({gameReviewList, showGameProfileLink}) => {
    return (
        <ul className={"mx-auto list-none"} >
            {gameReviewList.map((review) => (
                <li key={review.id}>
                    <GameReviewCard gameReview={review} showGameProfileLink={showGameProfileLink}/>
                </li>
            ))}
        </ul>

    )
}


export default GameReviewList;