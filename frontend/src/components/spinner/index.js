import React from "react";
import styled from "styled-components";


const SpinnerInner = styled.div`
  display: inline-block;
  width: 50px;
  height: 50px;
  border: 3px solid rgba(255,255,255,.3);
  border-radius: 50%;
  border-top-color: #000000;
  animation: spin 1s ease-in-out infinite;
  -webkit-animation: spin 1s ease-in-out infinite;

    @keyframes spin {
      to { transform: rotate(360deg); }
    }
    @-webkit-keyframes spin {
      to { transform: rotate(360deg); }
    }
`
const Spinner = () => {
    return (
        <div className="flex items-center justify-center mt-10">
            <SpinnerInner/>
        </div>
    );
}


export default Spinner;