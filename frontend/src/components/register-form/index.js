import React, {useState} from "react";
import RegisterForm from "./register-form";
import AuthService from "../../services/auth";
import UserProfileService from "../../services/user-profile";
import {getFormErrors} from "../../utils/form";
import UserRelationsService from "../../services/user-relations";


const VALIDATORS = {
    email: (data) => {
        if (data["email"].length < 1) {
            return ["To pole nie może być puste"]
        }
        if (data["email"].length > 50) {
            return ["To pole może mieć maksymalnie 50 znaków"]
        }
    },
    name: (data) => {
        if (data["name"].length < 1) {
            return ["To pole nie może być puste"]
        }
        if (data["name"].length > 50) {
            return ["To pole może mieć maksymalnie 50 znaków"]
        }
    },
    surname: (data) => {
        if (data["surname"].length < 1) {
            return ["To pole nie może być puste"]
        } if (data["surname"].length > 50) {
            return ["To pole może mieć maksymalnie 50 znaków"]
        }
    },
    password: (data) => {
        if (data["password"].length < 8) {
            return ["Hasło musi mieć przynajmniej 8 znaków"]
        } if (data["password"].length > 50) {
            return ["To pole może mieć maksymalnie 50 znaków"]
        }
    },
    password_repeat: (data) => {
        if (data["password"] !== data["password_repeat"]) {
            return ["Hasła nie zgadzają się"]
        }
    },

}


const RegisterFormContainer = () => {
    const [formErrors, setFormErrors] = useState({});
    const [isRegistered, setIsRegistered] = useState(false);

    const onRegister = (data) => {
        const {isValid, formErrors} = getFormErrors(VALIDATORS)(data);

        if (!isValid) {
            setFormErrors(formErrors);
            return;
        }

        const {
            email, name, surname, password
        } = data;

        AuthService.register(email, password)
            .then(data => {
                return UserProfileService.createProfile(name, surname, data.id);
            }).then(data => {
                return UserRelationsService.createRelations(data.id);
            }).then(() => setIsRegistered(true))

    }

    return (
        <RegisterForm
            onRegister={onRegister}
            formErrors={formErrors}
            isRegistered={isRegistered}
        />
    );
}


export default RegisterFormContainer;