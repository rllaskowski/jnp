import React, {useRef} from "react";
import FormErrors from "../form-errors";
import routes from "../../constant/routes";
import {Link} from "react-router-dom";


const RegisterForm = ({onRegister, formErrors, isRegistered}) => {
    const emailInputRef = useRef();
    const nameInputRef = useRef();
    const surnameInputRef = useRef();
    const passwordInputRef = useRef();
    const passwordRepeatInputRef = useRef();


    const onSubmit = (evt) => {
        evt.preventDefault();

        onRegister({
            email: emailInputRef.current.value,
            name: nameInputRef.current.value,
            surname: surnameInputRef.current.value,
            password: passwordInputRef.current.value,
            password_repeat: passwordRepeatInputRef.current.value
        });
    }

    if (isRegistered) {
        return (
            <div className={"max-w-screen-md mx-auto mt-10 shadow-xl p-10 card"}>
                <h1 className={"text-center font-bold text-xl"}>Rejestracja przebiegła pomyślnie</h1>
                <Link to={routes.LOGIN} className={"btn btn-primary mx-auto mt-3"}>Zaloguj się</Link>
            </div>
        )
    }

    return (
        <div className={"max-w-screen-md mx-auto mt-10 shadow-xl p-10 card"}>
            <form onSubmit={onSubmit}>
                <div className={"form-control"}>
                    <input
                        type={"email"} ref={emailInputRef}
                        placeholder="Adres e-mail"
                        className="input input-bordered mb-2"
                        maxLength={100}
                    />
                    <FormErrors errors={formErrors["email"] || []}/>
                </div>
                <div className={"form-control"}>
                    <input
                        type={"text"} ref={nameInputRef}
                        placeholder="Imię"
                        className="input input-bordered mb-2"
                        maxLength={50}
                    />
                    <FormErrors errors={formErrors["name"] || []}/>
                </div>
                <div className={"form-control"}>
                    <input
                        type={"text"} ref={surnameInputRef}
                        placeholder="Nazwisko"
                        className="input input-bordered mb-2 "
                        maxLength={50}
                    />
                    <FormErrors errors={formErrors["surname"] || []}/>
                </div>
                <div className={"form-control"}>
                    <input
                        type={"password"} ref={passwordInputRef}
                        placeholder="Hasło"
                        className="input input-bordered mb-2"
                        maxLength={50}
                    />
                    <FormErrors errors={formErrors["password"] || []}/>
                </div>
                <div className={"form-control"}>
                    <input
                        type={"password"} ref={passwordRepeatInputRef}
                        placeholder="Powtórz hasło"
                        className="input input-bordered mb-2"
                        maxLength={50}
                    />
                    <FormErrors errors={formErrors["password_repeat"] || []}/>
                </div>
                <div>
                    <button type={"submit"} className={"btn btn-primary btn-active w-full"}>
                        Zarejestruj się
                    </button>
                </div>
            </form>
        </div>
    );
}


export default RegisterForm;