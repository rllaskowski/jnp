import React from "react";
import useQueryParam from "../../utils/hooks/use-query-param";
import SearchForm from "./search-form";
import {useNavigate} from "react-router-dom";
import routes from "../../constant/routes";


const SearchFormContainer = () => {
    const searchedType = useQueryParam("type", "users");
    const type = searchedType === "games" || searchedType === "users"? searchedType : "users";
    const term = useQueryParam("term", "");
    const navigate = useNavigate();

    const onSearch = ({type, term}) => {
        if (term.trim() === "") {
            return;
        }
        const url = `${routes.SEARCH}?type=${type}&term=${term}`;
        navigate(url);
    }

    return (
        <SearchForm
            onSearch={onSearch}
            termDefault={term}
            typeDefault={type}
        />
    );
}

export default SearchFormContainer;