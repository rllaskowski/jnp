import React, {useRef, useState} from "react";


const SearchForm = ({onSearch, typeDefault, termDefault}) => {
    const termInputRef = useRef();
    const [type, setType] = useState(typeDefault);

    const onSubmit = (evt) => {
        evt.preventDefault();

        onSearch({
            term: termInputRef.current.value,
            type
        });
    }

    const onChangeType = (evt) => {
        setType(evt.target.value);
    }

    return (
        <div className={"mt-10"}>
            <form onSubmit={onSubmit} className={"flex justify-center"}>
                <input
                    type={"text"} placeholder={"Czego szukasz?"} defaultValue={termDefault}
                    className={"input input-bordered mr-1"}
                    ref={termInputRef}
                />

                <select className="select select-bordered w-full max-w-xs mr-1"
                        onChange={onChangeType} value={type}
                >
                    <option value={"games"}>w grach</option>
                    <option value={"users"}>w użytkownikach</option>
                </select>

                <button className={"btn btn-primary btn-active"}>Szukaj</button>
            </form>
        </div>
    );
}

export default  SearchForm;