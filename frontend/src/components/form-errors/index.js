import React from "react";


const FormErrors = ({errors}) => {
    return (
        <ul className={"mb-2"}>
            {errors.map((error) => (
                <li key={error} className={"text-red-600 text-xs"}>{error}</li>
            ))}
        </ul>

    );
}

export default FormErrors;