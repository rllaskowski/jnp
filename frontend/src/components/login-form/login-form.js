import React, {useRef} from "react";
import FormErrors from "../form-errors";


const LoginForm = ({onLogin, formErrors}) => {
    const emailInputRef = useRef();
    const passwordInputRef = useRef();

    const onSubmit = (evt) => {
        evt.preventDefault();

        onLogin({
            email: emailInputRef.current.value,
            password: passwordInputRef.current.value
        });
    }

    return (
        <div className={"max-w-screen-md mx-auto mt-10 shadow-xl p-10 card"}>
            <form onSubmit={onSubmit}>
                <div>
                    <FormErrors errors={formErrors["error"] || []}/>
                </div>
                <div className={"form-control"}>
                    <input
                        type={"text"} ref={emailInputRef}
                        placeholder="Adres e-mail"
                        className="input input-bordered mb-2"
                    />
                    <FormErrors errors={formErrors["email"] || []}/>
                </div>
                <div className={"form-control"}>
                    <input
                        type={"password"}
                        ref={passwordInputRef}
                        placeholder="Hasło"
                        className="input input-bordered mb-2"
                    />
                    <FormErrors errors={formErrors["password"] || []}/>
                </div>
                <div>
                    <button type={"submit"} className={"btn btn-primary btn-active w-full"}>
                        Zaloguj się
                    </button>
                </div>
            </form>
        </div>
    );
}


export default LoginForm;