import React, {useEffect, useState} from "react";
import LoginForm from "./login-form";
import {useDispatch} from "react-redux";
import AuthService from "../../services/auth";
import {login} from "../../store/auth";


const LoginFormContainer = () => {
    const dispatch = useDispatch();
    const [formErrors, setFormErrors] = useState({});

    const onLogin = ({email, password}) => {
        AuthService.login(email, password)
            .then(() => {
                dispatch(login());
            }).catch((error) => {
                const response = error.response;
                const data = response.data;

                setFormErrors(data);
            })
    }

    return (
        <LoginForm
            onLogin={onLogin}
            formErrors={formErrors}
        />
    );
}


export default LoginFormContainer;