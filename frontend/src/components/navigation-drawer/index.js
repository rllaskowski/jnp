import React, {useCallback} from "react";
import NavigationDrawer from "./navigation-drawer";
import {useDispatch, useSelector} from "react-redux";
import {isLoggedInSelector, logout} from "../../store/auth";
import AuthService from "../../services/auth";


const NavigationDrawerContainer = ({...props}) => {
    const isLoggedIn = useSelector(isLoggedInSelector);
    const dispatch = useDispatch();

    const onLogout = useCallback(() => {
        if (!isLoggedIn) { return; }
        AuthService.logout();
        dispatch(logout());
    }, [isLoggedIn]);

    return (
        <NavigationDrawer
            isLoggedIn={isLoggedIn}
            onLogout={onLogout}
            {...props}
        />
    )
}


export default NavigationDrawerContainer;