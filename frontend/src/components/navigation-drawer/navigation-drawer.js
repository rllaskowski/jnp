import React from "react";
import {Link} from "react-router-dom";
import routes from "../../constant/routes";


const Navigation = ({children}) => {
    return (
        <div className="navbar mb-2 shadow-lg bg-neutral text-neutral-content">
            <div className="flex-1 px-2 mx-2">
                <Link to={routes.FEED} className="text-lg font-bold">
                    GameReviewer
                </Link>
            </div>
            <div className="flex-none lg:hidden">
                <label htmlFor="my-drawer-3" className="btn btn-square btn-ghost">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                         className="inline-block w-6 h-6 stroke-current">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                              d="M4 6h16M4 12h16M4 18h16"/>
                    </svg>
                </label>
            </div>
            {children}
        </div>
    )
}

const MenuItem = ({children, ...props}) => {
    const { to, ...propsRest } = props;

    if (to) {
        return (
            <Link to={to} {...propsRest}>
                {children}
            </Link>
        )
    }

    return (
        <span {...propsRest}>
            {children}
        </span>
    )
}

const NavigationMenuItem = ({children, ...props}) => {
    return  (
        <MenuItem className={"btn btn-ghost btn-sm rounded-btn"} {...props}>
            {children}
        </MenuItem>
    );
}


const DrawerMenuItem = ({children, ...props}) => {
    return  (
        <li>
             <MenuItem className={"rounded-btn btn-sm"} {...props}>
                {children}
            </MenuItem>
        </li>
    );
}

const MenuList = ({isLoggedIn, onLogout, MenuItem}) => {

    return (
        <>
            <MenuItem to={routes.SEARCH}>Wyszukaj</MenuItem>
            {isLoggedIn?
                <>
                    <MenuItem to={routes.EDIT_PROFILE}>Mój profil</MenuItem>
                    <MenuItem onClick={onLogout}>Wyloguj</MenuItem>
                </> :
                <>
                    <MenuItem to={routes.REGISTER}>Zarejestuj się</MenuItem>
                    <MenuItem to={routes.LOGIN}>Zaloguj się</MenuItem>
                </>
            }
        </>
    )
}


const NavigationDrawer = ({children, isLoggedIn, onLogout}) => {

    return (
        <div className="drawer">
            <input id="my-drawer-3" type="checkbox" className="drawer-toggle"/>
            <div className="flex flex-col drawer-content min-h-screen">
                <Navigation>
                    <div className="flex-none hidden lg:block">
                        <ul className="menu horizontal">
                            <MenuList
                                isLoggedIn={isLoggedIn}
                                onLogout={onLogout}
                                MenuItem={NavigationMenuItem}
                            />
                        </ul>
                    </div>
                </Navigation>
                {children}
            </div>
            <div className="drawer-side">
                <label htmlFor="my-drawer-3" className="drawer-overlay"/>
                <ul className="p-4 overflow-y-auto menu w-80 bg-base-100">
                    <MenuList
                        isLoggedIn={isLoggedIn}
                        onLogout={onLogout}
                        MenuItem={DrawerMenuItem}
                    />
                </ul>
            </div>
        </div>
    );
}


export default NavigationDrawer;