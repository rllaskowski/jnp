import React, {useState} from "react";

const FileUploadInput = ({onChange}) => {
    const [fileName, setFileName] = useState();

    const onInputChange = (evt) => {
        const files = evt.target.files;
        if (files.length > 0) {
            setFileName(files[0].name);
            onChange(files[0]);
        }
    }

    return (
        <div className="flex justify-center mt-8">
            <div className="max-w-2xl rounded-lg shadow-xl bg-gray-50">
                <div className="m-4">
                    <div className="flex items-center justify-center w-full">
                        <label
                            className="flex flex-col w-full h-32 border-4 border-blue-200 border-dashed hover:bg-gray-100 hover:border-gray-300">
                            <div className="flex flex-col items-center justify-center pt-7">
                                <svg xmlns="http://www.w3.org/2000/svg"
                                     className="w-8 h-8 text-gray-400 group-hover:text-gray-600"
                                     fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                          d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12"/>
                                </svg>
                                <p className="pt-1 text-sm tracking-wider text-gray-400 group-hover:text-gray-600">
                                    {fileName === undefined? "Dodaj nowe zdjęcie" : fileName}
                                </p>
                            </div>
                            <input type="file" className="opacity-0" onChange={onInputChange}/>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    );
}

const UploadImageModal = ({onImageUpload, buttonTitle}) => {
    const [file, setFile] = useState();

    const onInputChange = (file) => {
        setFile(file);
    }

    const onUpload = () => {
        if (file) {
            onImageUpload(file);
        }
    }

    return (
        <>
            <label htmlFor="my-modal" className="btn btn-primary btn-outline w-1/2 mx-auto my-5 modal-button">
                {buttonTitle}
            </label>
            <input type="checkbox" id="my-modal" className="modal-toggle"/>
            <div className="modal">
                <div className="modal-box">
                    <FileUploadInput onChange={onInputChange}/>
                    <div className="modal-action">
                        <label
                            htmlFor="my-modal" className="btn btn-primary"
                            onClick={onUpload}
                        >
                            Wyślij
                        </label>
                        <label htmlFor="my-modal" className="btn">Anuluj</label>
                    </div>
                </div>
            </div>
        </>
    );
}

export default UploadImageModal;