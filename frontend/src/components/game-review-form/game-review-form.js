import React, {useState, useRef} from "react";
import ReactStars from "react-stars/dist/react-stars";
import UploadImageModal from "../upload-image-modal";

const GameReviewForm = ({onSubmitReview}) => {
    const [score, setScore] = useState();
    const titleInputRef = useRef()
    const textInputRef = useRef();
    const [image, setImage] = useState();

    const onSubmit = (evt) => {
        evt.preventDefault();

        onSubmitReview({
            image,
            title: titleInputRef.current.value,
            score,
            text: textInputRef.current.value
        });
    }

    return (
        <div className={"max-w-screen-md my-5 mx-auto shadow-xl card"}>
            <div className={"card-body"}>
                <form onSubmit={onSubmit}>
                    <div className="form-control mb-2">
                        <input
                            placeholder="Tytuł recenzji" className="input input-lg input-bordered" type="text"
                            ref={titleInputRef}
                        />
                    </div>
                    <div>
                        <textarea
                            ref={textInputRef}
                            placeholder={"Co sądzisz o tej grze?"} className={"textarea h-20 textarea-bordered w-full"}
                        />
                    </div>
                    <div className={"flex justify-center my-2"}>
                        <ReactStars value={score} count={10} size={30} half={false} onChange={setScore}/>
                    </div>
                    <div className={"flex justify-center"}>
                        <UploadImageModal buttonTitle={"Dodaj zdjęcie recenzji"} onImageUpload={setImage}/>
                    </div>
                    <div>
                        <button className={"btn btn-active btn-primary w-full"}>
                            Wyślij recenzję
                        </button>
                    </div>
                </form>
            </div>

        </div>

    );
}


export default GameReviewForm;