import React, {useState} from "react";
import GameReviewForm from "./game-review-form";
import GameReviewService from "../../services/game-reviews";


const GameReviewFormContainer = ({gameId, onSubmitReview}) => {
    const [isPosting, setIsPosting] = useState(false);

    const onSubmit = ({text, score, title, image}) => {
        if (text.trim().length === 0 || score === undefined ||
            title.trim().length === 0 ||image === undefined) { return; }

        GameReviewService.addReview({
            score, game_id: gameId, description: text, title
        }).then((data) =>
            GameReviewService.uploadImage(image, data.id)
        ).then(() => onSubmitReview())
    }

    return (
        <GameReviewForm
            onSubmitReview={onSubmit}
            isPosting={isPosting}
        />
    );
}


export default GameReviewFormContainer;