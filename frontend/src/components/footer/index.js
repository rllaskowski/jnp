import React from "react";


const Footer = () => {
    return (
        <footer className="p-10 footer bg-neutral text-neutral-content mb-0 mt-auto footer-center">
            <div>
                <p className="font-bold">
                    JNP3 - MIKROSERWISY
                </p>
                <p>Copyright © Robert Laskowski, Michał Orzyłowski 2022</p>
            </div>
        </footer>
    )
}


export default Footer;