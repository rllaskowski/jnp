import React from "react";


const ContentWrapper = ({children, className, ...props}) => {
    const classNameCombined = `container mx-auto ${className? className : ""}`;

    return (
        <div {...props} className={classNameCombined}>
            {children}
        </div>
    );
}

export default ContentWrapper;