import React, {useEffect, useState} from "react";
import GameReviewCard from "./game-review-card";
import GameProfileService from "../../services/game-profile";
import UserProfileService from "../../services/user-profile";


const GameReviewCardContainer = ({gameReview, showGameProfileLink}) => {
    console.log("review", gameReview)
    const [gameProfile, setGameProfile] = useState()
    const [userProfile, setUserProfile] = useState();

    useEffect(() => {
        GameProfileService.getGameProfile(gameReview.game_id)
            .then((gameProfile) => setGameProfile(gameProfile))
    }, [gameReview.game_id])

    useEffect(() => {
        UserProfileService.getUserProfile(gameReview.user_id)
            .then(data => setUserProfile(data));
    }, [gameReview.user_id]);

    return (
      <GameReviewCard
          gameReview={gameReview}
          gameProfile={gameProfile}
          userProfile={userProfile}
          showGameProfileLink={showGameProfileLink}
      />
    );
}


export default GameReviewCardContainer;