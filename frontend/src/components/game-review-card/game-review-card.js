import React from "react";
import {Link, useNavigate} from "react-router-dom";
import routes from "../../constant/routes";
import Spinner from "../spinner";
import GameReviewsService from "../../services/game-reviews";
import UserProfileService from "../../services/user-profile";
import ReactStars from "react-stars";

const onAuthorImageError = (evt) => {
    evt.target.src = "/img/default-profile-picutre.jpg";
}


const AuthorInfo = ({author}) => {
    const imageUrl = UserProfileService.getUserImage(author.id);
    const profileUrl = routes.USER_PROFILE.replace(":userId", author.id);

    return (
        <Link className="avatar ml-4" to={profileUrl}>
            <div className="mb-8 rounded-full w-16 h-16 border">
                <img src={imageUrl} alt={"Profile picture"} onError={onAuthorImageError}/>
            </div>
            <div className={"font-bold ml-2"}>
                {`${author.name} ${author.surname}`}
            </div>
        </Link>
    )
}

const onGameReviewImageError = (evt) => {
    evt.target.src = ""
}

const GameReviewCard = ({gameProfile, userProfile, gameReview, showGameProfileLink}) => {
    const imageUrl = GameReviewsService.getImage(gameReview.id);

    const gameLink = routes.GAME_PROFILE
        .replace(":gameId", gameReview.game_id);

    if (!gameProfile || !gameReview || !userProfile) {
        return (
            <Spinner/>
        )
    }

    return (
        <div className="card  shadow-xl px-4 py-10 max-w-screen-md mx-auto my-5">
            <figure className={"my-auto rounded max-h-96 overflow-hidden mx-auto"}>
                <img
                    src={imageUrl} alt={"Review image"} className={"rounded w-full"}
                    onError={onGameReviewImageError}
                />
            </figure>

            <div className="card-body ">
                <div className={"flex"}>
                    <div>
                        <h2 className="card-title">{gameProfile.name}</h2>
                        <ReactStars value={gameReview.score} count={10} size={25} half={false} edit={false}/>
                    </div>
                    <AuthorInfo author={userProfile}/>
                </div>
                <h3 className={"text-lg font-bold"}>{gameReview.title}</h3>
                <p>{gameReview.description}</p>
                {showGameProfileLink &&
                    <div className="card-actions">
                        <Link to={gameLink} className="btn btn-primary">Zobacz grę</Link>
                    </div>
                }

            </div>
        </div>
    );
}


export default GameReviewCard;