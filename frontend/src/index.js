import React from 'react';
import "./index.css";
import ReactDOM from 'react-dom';
import TokenProvider from "./client/token-provider";
import userProfileService from "./services/user-profile";
import authService from "./services/auth";
import userRelationsService from "./services/user-relations";
import gameReviewsService from "./services/game-reviews";
import reportWebVitals from './reportWebVitals';
import App from "./app";
import createReducer from "./store";
import {Provider} from "react-redux";
import {createStore, compose} from "redux";
import {DEBUG, ACCESS_TOKEN_STORAGE_KEY, REFRESH_TOKEN_STORAGE_KEY} from "./config";


const tokenProvider = new TokenProvider(
    ACCESS_TOKEN_STORAGE_KEY,
    REFRESH_TOKEN_STORAGE_KEY
);

authService.client.setTokenProvider(tokenProvider);
userProfileService.client.setTokenProvider(tokenProvider);
userRelationsService.client.setTokenProvider(tokenProvider);
userRelationsService.client.setTokenProvider(tokenProvider);
gameReviewsService.client.setTokenProvider(tokenProvider);

const composeEnhancers = DEBUG?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose : compose
const reducer = createReducer();
const store = createStore(reducer, undefined, composeEnhancers());


ReactDOM.render(
  <React.StrictMode>
      <Provider store={store}>
          <App />
      </Provider>

  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
