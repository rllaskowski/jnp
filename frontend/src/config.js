export const DEBUG = process.env.REACT_APP_DEBUG;

export const ACCESS_TOKEN_STORAGE_KEY = process.env.REACT_APP_ACCESS_TOKEN_STORAGE_KEY || "access_token";

export const REFRESH_TOKEN_STORAGE_KEY = process.env.REACT_APP_REFRESH_TOKEN_STORAGE_KEY || "refresh_token"

export const DEFAULT_CACHE_TTL = process.env.REACT_APP_DEFAULT_CACHE_TTL || 1000;

export const AUTH_HOST = process.env.REACT_APP_AUTH_HOST;

export const GAME_PROFILE_HOST = process.env.REACT_APP_GAME_PROFILE_HOST;

export const USER_PROFILE_HOST = process.env.REACT_APP_USER_PROFILE_HOST;

export const GAME_REVIEWS_HOST = process.env.REACT_APP_GAME_REVIEWS_HOST;

export const USER_RELATIONS_HOST = process.env.REACT_APP_USER_RELATIONS_HOST;

