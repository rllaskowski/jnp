import Service from "./service";
import {GAME_REVIEWS_HOST} from "../config";


const GAME_REVIEW_LIST_URL = "/reviews/list";
const ADD_GAME_REVIEW_URL = "/reviews/authenticated";
const UPLOAD_GAME_REVIEW_IMAGE_URL = "/reviews/<game_review_id>/image";
const GAME_REVIEW_IMAGE_URL = "/reviews/<game_review_id>/image";


class GameReviewsService extends Service {
    getGameReviewList(filters, count, offset) {
        const params = {
            ...filters,
        };

        return this.client.get(GAME_REVIEW_LIST_URL, params, false);
    }

    addReview(reviewData) {
        return this.client.post(ADD_GAME_REVIEW_URL, reviewData);
    }

    uploadImage(image, gameReviewId) {
        const url = UPLOAD_GAME_REVIEW_IMAGE_URL.replace("<game_review_id>", gameReviewId);
        const formData = new FormData()
        formData.append('image', image);

        return this.client.post(url, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
    }

    getImage(gameReviewId) {
        return `${this.hostUri}${GAME_REVIEW_IMAGE_URL.replace("<game_review_id>", gameReviewId)}`;
    }
}


export default new GameReviewsService(GAME_REVIEWS_HOST);