import Service from "./service";
import {AUTH_HOST} from "../config";


const REGISTER_URL = "/register"
const TOKEN_VALIDATE_URL = "/validate_token";

class AuthService extends Service {

    login(email, password) {
        return this.client.tokenProvider.authenticate(email, password);
    }

    logout() {
        this.client.tokenProvider.removeTokens();
    }

    register(email, password) {
        const data = {
            email,
            password
        };

        return this.client.post(REGISTER_URL, data);
    }

    validateToken() {
        return this.client.post(TOKEN_VALIDATE_URL);
    }
}

export default new AuthService(AUTH_HOST);