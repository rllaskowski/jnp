import ApiClient from "../client";


class Service {
    constructor(hostUri) {
        this.hostUri = hostUri;
        this.client = new ApiClient(hostUri);
    }
}

export default Service;