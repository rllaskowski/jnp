import Service from "./service";
import {USER_PROFILE_HOST} from "../config";

const USER_PROFILE_CREATE_URL = "/user_profiles";
const USER_PROFILE_EDIT_URL = "/user_profiles/authenticated";
const USER_PROFILE_IMAGE_UPLOAD_URL = "/user_profiles/authenticated/image";
const USER_PROFILE_AUTHENTICATED_URL = "/user_profiles/authenticated"
const USER_PROFILE_SINGLE_URL = "/user_profiles/<user_id>";
const USER_PROFILE_LIST_URL = "/user_profiles/list";
const USER_PROFILE_IMAGE_URL = "/user_profiles/<user_id>/image"
const USER_PROFILE_THUMBNAIL_URL = "/user_profiles/<user_id>/thumbnail"

class UserProfileService extends Service {
    createProfile(name, surname, id) {
        const data = {
            name, surname, id
        }

        return this.client.post(USER_PROFILE_CREATE_URL, data);
    }

    editProfile(data) {
        return this.client.patch(USER_PROFILE_EDIT_URL, data);
    }

    getAuthenticatedUserProfile() {
        return this.client.get(USER_PROFILE_AUTHENTICATED_URL);
    }


    getUserProfile(userId) {
        return this.client.get(USER_PROFILE_SINGLE_URL.replace("<user_id>", userId),
            {}, false);
    }

    getUserProfileList(filters, count, offset) {
        const params = {
            ...filters,
            count,
            offset
        };

        return this.client.get(USER_PROFILE_LIST_URL, params, false);
    }

    getUserImage(userId) {
        return `${this.hostUri}${USER_PROFILE_IMAGE_URL.replace("<user_id>", userId)}`;
    }

    getUserThumbnail(userId) {
        return `${this.hostUri}${USER_PROFILE_THUMBNAIL_URL.replace("<user_id>", userId)}`;
    }

    uploadImage(file) {
        const formData = new FormData()
        formData.append('image', file);

        return this.client.post(USER_PROFILE_IMAGE_UPLOAD_URL, formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        });
    }
}


export default new UserProfileService(USER_PROFILE_HOST);