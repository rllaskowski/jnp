import Service from "./service";
import {GAME_PROFILE_HOST} from "../config";


const GAME_PROFILE_SINGLE_URL = "/game_profiles/<game_id>";
const GAME_PROFILE_LIST_URL = "/game_profiles/list";
const GAME_PROFILE_IMAGE_URL = "/game_profiles/<game_id>/image"
const GAME_PROFILE_THUMBNAIL_URL = "/game_profiles/<game_id>/thumbnail"


class GameProfileService extends Service {
    getGameProfile(gameId) {
        return this.client.get(GAME_PROFILE_SINGLE_URL.replace("<game_id>", gameId),
            {}, false);
    }

    getGameProfileList(filters, count, offset) {
        const params = {
            ...filters,
            count,
            offset
        };

        return this.client.get(GAME_PROFILE_LIST_URL, params, false);
    }

    getGameImage(gameId) {
        return `${this.hostUri}${GAME_PROFILE_IMAGE_URL.replace("<game_id>", gameId)}`;
    }

    getGameThumbnail(gameId) {
        return `${this.hostUri}${GAME_PROFILE_THUMBNAIL_URL.replace("<game_id>", gameId)}`;
    }
}


export default new GameProfileService(GAME_PROFILE_HOST);