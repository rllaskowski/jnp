import Service from "./service";
import {USER_RELATIONS_HOST} from "../config";


const USER_RELATIONS_CREATE_URL = "/relations";
const USER_RELATIONS_FOLLOWED_URL = "/relations";
const USER_RELATIONS_FOLLOW_URL = "/relations/add_relation";
const USER_RELATIONS_UNFOLLOW_URL = "/relations/remove_relation";
const USER_RELATIONS_IS_FOLLOWED = "/relations/is_followed";


class UserRelationsService extends Service {

    createRelations(userId) {
        const data = {
            id: userId
        }

        return this.client.post(USER_RELATIONS_CREATE_URL, data);
    }

    followUser(userId) {
        const data = {user_id: userId};

        return this.client.patch(USER_RELATIONS_FOLLOW_URL, data);
    }

    unfollowUser(userId) {
        const data = {user_id: userId};

        return this.client.patch(USER_RELATIONS_UNFOLLOW_URL, data);
    }

    followGame(gameId) {
        const data = {game_id: gameId};

        return this.client.patch(USER_RELATIONS_FOLLOW_URL, data);
    }

    unfollowGame(gameId) {
        const data = {game_id: gameId};

        return this.client.patch(USER_RELATIONS_UNFOLLOW_URL, data);
    }

    getFollowed() {
        return this.client.get(USER_RELATIONS_FOLLOWED_URL);
    }

    isFollowedUser(userId) {
        const params = {user_id: userId};

        return this.client.get(USER_RELATIONS_IS_FOLLOWED, params);
    }

    isFollowedGame(gameId) {
        const params = {game_id: gameId};

        return this.client.get(USER_RELATIONS_IS_FOLLOWED, params);
    }
}


export default new UserRelationsService(USER_RELATIONS_HOST);