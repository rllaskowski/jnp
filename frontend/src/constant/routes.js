const routes = {
    FEED: "/",
    LOGIN: "/zaloguj",
    SEARCH: "/szukaj",
    REGISTER: "/zarejestruj",
    GAME_PROFILE: "/gra/:gameId",
    USER_PROFILE: "/uzytkownik/:userId",
    EDIT_PROFILE: "/moj-profil"
};

export default routes;