export const getFormErrors = (validators) => (data) => {
    let isValid = true;
    const formErrors = {};

    for (let [key, value] of Object.entries(data)) {
        const errors = validators[key](data) || [];
        if (errors.length > 0) {
            isValid = false;
            formErrors[key] = errors;
        }
    }

    return {
        isValid,
        formErrors
    }
}

