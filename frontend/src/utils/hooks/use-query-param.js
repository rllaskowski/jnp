import { useMemo } from "react";
import queryString from "query-string";
import { useLocation } from "react-router-dom";

const useQueryParam = (queryParam, defaultValue=undefined) => {
    const location = useLocation();

    return useMemo(() =>
            queryString.parse(location.search)[queryParam] || defaultValue
        , [location.search, queryParam])
}

export default useQueryParam;