import {prop} from "ramda";
import {createSelector} from "reselect";
import {Map} from "immutable";


export const AUTH_REDUCER_NAME = "AUTH_REDUCER";

const LOGIN = `${AUTH_REDUCER_NAME}/LOGIN`;

const LOGOUT = `${AUTH_REDUCER_NAME}/LOGOUT`;

export const logout = () => {
    return {
        type: LOGOUT,
        payload: {}
    }
}

export const login = () => {
    return {
        type: LOGIN,
        payload: {}
    }
}


const initialState = Map({
    isLoggedIn: undefined,
    displayName: undefined
});


export const authReducer = (state=initialState, action) => {
    switch (action.type) {
        case LOGIN:
            return state
                .set("isLoggedIn", true)
                .set("displayName", action.payload.displayName);
        case LOGOUT:
            return state
                .set("isLoggedIn", false)
                .set("displayName", undefined);
        default:
            return state;
    }
}


const getAuthReducerState = prop(AUTH_REDUCER_NAME);

export const isLoggedInSelector = createSelector(
    getAuthReducerState,
    state => state.get("isLoggedIn")
);

export const displayNameSelector = createSelector(
    getAuthReducerState,
    state => state.get("displayName")
);
