import {combineReducers} from "redux";
import {
    AUTH_REDUCER_NAME,
    authReducer
} from "./auth";


const createReducer = () => {
    return combineReducers({
        [AUTH_REDUCER_NAME]: authReducer
    })
}


export default createReducer;