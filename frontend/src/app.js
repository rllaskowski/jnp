import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {
    BrowserRouter as Router,
    Route, Routes
} from "react-router-dom";
import UserProfileView from "./views/user-profile-view";
import GameProfileView from "./views/game-profile-view";
import SearchView from "./views/search-view";
import EditProfileView from "./views/edit-profile-view";
import LoginView from "./views/login-view";
import FeedView from "./views/feed-view";
import RegisterView from "./views/register-view/register-view";
import AuthService from "./services/auth";
import routes from "./constant/routes";
import {isLoggedInSelector, login, logout} from "./store/auth";
import Spinner from "./components/spinner";


const App = () => {
    const dispatch = useDispatch();
    const isLoggedIn = useSelector(isLoggedInSelector);

    useEffect(() => {
        AuthService.validateToken()
            .then(() => {
                dispatch(login())
            }).catch(() => {
                AuthService.logout();
                dispatch(logout());
            })
    }, []);

    if (isLoggedIn === undefined) {
        return (
            <Spinner/>
        );
    }

    return (
        <Router>
            <Routes>
                <Route
                    exact={true} path={routes.FEED}
                    element={<FeedView/>}
                />
                <Route
                    exact={true} path={routes.SEARCH}
                    element={<SearchView/>}
                />
                <Route
                    exact={true} path={routes.EDIT_PROFILE}
                    element={<EditProfileView/>}
                />
                <Route
                    exact={true} path={routes.USER_PROFILE}
                    element={<UserProfileView/>}
                />
                <Route
                    exact={true} path={routes.GAME_PROFILE}
                    element={<GameProfileView/>}
                />
                <Route
                    exact={true} path={routes.LOGIN}
                    element={<LoginView/>}
                />
                <Route
                    exact={true} path={routes.REGISTER}
                    element={<RegisterView/>}
                />
            </Routes>
        </Router>
    );
}


export default App;