import React from "react";
import Footer from "../../components/footer";
import NavigationDrawer from "../../components/navigation-drawer";
import RegisterForm from "../../components/register-form";
import ContentWrapper from "../../components/content-wrapper";


const RegisterView = () => {
    return (
        <NavigationDrawer>
            <ContentWrapper>
                <RegisterForm/>
            </ContentWrapper>
            <Footer/>
        </NavigationDrawer>
    )
}


export default RegisterView;