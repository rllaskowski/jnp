import React from "react";
import {useSelector} from "react-redux";
import {isLoggedInSelector} from "../../store/auth";
import {Navigate} from "react-router-dom";
import routes from "../../constant/routes";
import RegisterView from "./register-view";


const RegisterViewContainer = () => {
    const isLoggedIn = useSelector(isLoggedInSelector);

    if (isLoggedIn) {
        return (
            <Navigate to={routes.FEED}/>
        );
    }

    return (
        <RegisterView/>
    )
}


export default RegisterViewContainer;