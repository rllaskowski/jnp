import React from "react";
import Footer from "../../components/footer";
import NavigationDrawer from "../../components/navigation-drawer";
import ContentWrapper from "../../components/content-wrapper";
import Spinner from "../../components/spinner";
import GameReviewList from "../../components/game-review-list";
import GameReviewForm from "../../components/game-review-form";



const GameInfoCard = ({gameProfile, onFollowToggle, isFollowed, gameImage}) => {

    return (
        <div className="card text-center shadow-lg max-w-screen-md my-5 mx-auto">
            <figure className="px-10 pt-10">
                <img src={gameImage || "https://sm.ign.com/t/ign_pl/screenshot/default/witcher-3-story-1920x1075-1_15wc.1280.jpg"}
                     className="rounded-xl" alt={"Game profile image"}/>
            </figure>
            <div className="card-body">
                <h2 className="card-title">{gameProfile.name}</h2>
                <p>{gameProfile.description}</p>
                <div className="justify-center card-actions">
                    {isFollowed !== undefined &&
                        <button
                            className={`btn ${!isFollowed? "btn-outline" : ""} btn-accent`}
                            onClick={onFollowToggle}
                        >
                            {isFollowed? "Nie obserwuj" : "Obserwuj"}
                        </button>
                    }
                </div>
            </div>
        </div>
    );
}


const GameProfileView = (props) => {
    const {
        gameProfile, reviewList, onFollowToggle, isFollowed,
        isLoggedIn, onSubmitReview, gameImage, showReviewForm
    } = props;

    return (
        <NavigationDrawer>
            <ContentWrapper>
                {gameProfile === undefined?
                    <Spinner/> :
                    <GameInfoCard
                        gameProfile={gameProfile}
                        onFollowToggle={onFollowToggle}
                        isFollowed={isFollowed}
                        gameImage={gameImage}
                    />
                }
                {isLoggedIn && gameProfile !== undefined && showReviewForm &&
                    <GameReviewForm
                        gameId={gameProfile.id}
                        onSubmitReview={onSubmitReview}
                    />
                }
                {reviewList === undefined?
                    <Spinner/> :
                    reviewList.length === 0?
                        <h2 className={"text-xl font-bold text-center my-5"}>
                            Nikt nie napisał jeszcze recenzji tej gry. Możesz być pierwszy!
                        </h2> :
                        <GameReviewList gameReviewList={reviewList}/>
                }
            </ContentWrapper>
            <Footer/>
        </NavigationDrawer>
    );
}


export default GameProfileView;