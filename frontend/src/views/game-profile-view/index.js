import React, {useCallback, useEffect, useState} from "react";
import GameProfileService from "../../services/game-profile";
import GameProfileView from "./game-profile-view";
import GameReviewsService from "../../services/game-reviews";
import { useParams } from "react-router-dom";
import UserRelationsService from "../../services/user-relations";
import {useSelector} from "react-redux";
import {isLoggedInSelector} from "../../store/auth";


const GameProfileViewContainer = () => {
    const { gameId } = useParams();
    const [gameProfile, setGameProfile] = useState(undefined);
    const [reviewList, setReviewList] = useState(undefined);
    const [showReviewForm, setShowReviewForm] = useState(true);
    const [isFollowed, setIsFollowed] = useState(undefined);
    const isLoggedIn = useSelector(isLoggedInSelector);
    const gameImage = GameProfileService.getGameImage(gameId);


    useEffect(() => {
        setGameProfile(undefined);
        setReviewList(undefined);
        setIsFollowed(undefined);

        GameProfileService.getGameProfile(gameId)
            .then((gameProfile) => setGameProfile(gameProfile))

        GameReviewsService.getGameReviewList({
            game_id: gameId
        }, -1, 0).then((data) => {
            setReviewList(data.review_list);
        });
        if (isLoggedIn) {
            UserRelationsService.isFollowedGame(gameId)
                .then((data) => setIsFollowed(data.is_followed))
        }

    }, [gameId, isLoggedIn]);

    const onFollowToggle = useCallback(() => {
        if (!isLoggedIn || isFollowed === undefined) {
            return;
        }
        setIsFollowed(isFollowed => !isFollowed);

        if (!isFollowed) {
            UserRelationsService.followGame(gameId)
        } else {
            UserRelationsService.unfollowGame(gameId);
        }
    }, [isFollowed, gameId, isLoggedIn]);

    const onSubmitReview = () => {
        GameReviewsService.getGameReviewList({
            game_id: gameId
        }, null, 0).then((data) => {
            setShowReviewForm(false);
            setReviewList(data.review_list);
        });
    }

    return (
        <GameProfileView
            gameImage={gameImage}
            gameProfile={gameProfile}
            isFollowed={isFollowed}
            onFollowToggle={onFollowToggle}
            reviewList={reviewList}
            isLoggedIn={isLoggedIn}
            onSubmitReview={onSubmitReview}
            showReviewForm={showReviewForm}
        />
    )
}


export default GameProfileViewContainer;