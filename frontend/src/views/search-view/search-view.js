import React from "react";
import NavigationDrawer from "../../components/navigation-drawer";
import ContentWrapper from "../../components/content-wrapper";
import SearchForm from "../../components/search-form";
import Footer from "../../components/footer";
import Spinner from "../../components/spinner";
import GameProfileService from "../../services/game-profile";
import UserProfileService from "../../services/user-profile";
import routes from "../../constant/routes";
import {Link} from "react-router-dom";


const GameProfileSummaryCard = ({gameProfile}) => {
    const thumbnailUrl = GameProfileService.getGameImage(gameProfile.id);
    const gameProfileUrl = routes.GAME_PROFILE.replace(":gameId", gameProfile.id);

    return (
        <div className={"card card-side card-bordered max-w-screen-md shadow-lg mx-auto my-5"}>
            <figure className={"w-1/2"}>
                <img src={thumbnailUrl} alt={"Game thumbnail"} className={"w-full"}/>
            </figure>
            <div className="card-body">
                <h2 className="card-title">{gameProfile.name}</h2>
                <p>{gameProfile.description}</p>
                <div className="card-actions">
                    <Link className="btn btn-primary" to={gameProfileUrl}>Zobacz więcej</Link>
                </div>
            </div>
        </div>
    );
}

const UserProfileSummaryCard = ({userProfile}) => {
    const imageUrl = UserProfileService.getUserImage(userProfile.id);
    const userProfileUrl = routes.USER_PROFILE.replace(":userId", userProfile.id);

    const onNoImageError = (evt) => {
        evt.target.src = "/img/default-profile-picture.jpg";
    }

    return (
        <div className={"card card-side card-bordered max-w-screen-md shadow-lg mx-auto my-5"}>
            <figure className={"my-auto ml-5"}>
                <div className="avatar">
                    <div className={"w-28 h-28 rounded-full border"}>
                        <img src={imageUrl} alt={"User profile picture"} onError={onNoImageError}/>
                    </div>
                </div>
            </figure>
            <div className="card-body">
                <h2 className="card-title">{userProfile.name}</h2>
                <p>{userProfile.description}</p>
                <div className="card-actions">
                    <Link className="btn btn-primary" to={userProfileUrl}>Zobacz więcej</Link>
                </div>
            </div>
        </div>
    );
}


const UserProfileList = ({userProfileList}) => {
    return (
        <ul className={"list-none"}>
            {userProfileList.map((userProfile) => (
                <li key={userProfile.id}>
                    <UserProfileSummaryCard userProfile={userProfile}/>
                </li>
            ))}
        </ul>
    )
}


const GameProfileList = ({gameProfileList}) => {
    return (
        <ul className={"list-none"}>
            {gameProfileList.map((gameProfile) => (
                <li key={gameProfile.id}>
                    <GameProfileSummaryCard gameProfile={gameProfile}/>
                </li>
            ))}
        </ul>
    )
}


const SearchView = ({type, resultList}) => {
    return (
        <NavigationDrawer>
            <ContentWrapper>
                <SearchForm/>
                <div className={"mt-5"}>
                    {resultList === undefined?
                        <Spinner/> :
                    resultList === null?
                        <h1 className={"text-center text-xl font-bold"}>
                            Wpisz frazę i kliknij szukaj
                        </h1> :
                    resultList.length === 0?
                        <h1 className={"text-center text-xl font-bold"}>
                            Nie znaleziono wyników dla podanej frazy
                        </h1> :
                    resultList.length > 0 &&
                        type === "users"?
                        <UserProfileList userProfileList={resultList}/> :
                        <GameProfileList gameProfileList={resultList}/>
                    }
                </div>
            </ContentWrapper>
            <Footer/>
        </NavigationDrawer>
    )
}


export default SearchView;