import React, {useEffect, useState} from "react";
import SearchView from "./search-view";
import useQueryParam from "../../utils/hooks/use-query-param";
import GameProfileService from "../../services/game-profile";
import UserProfileService from "../../services/user-profile"


const SearchViewContainer = () => {
    const term = useQueryParam("term", "");
    const searchedType = useQueryParam("type", "users");
    const type = searchedType === "games" || searchedType === "users"? searchedType : "users";
    const [resultList, setResultList] = useState(undefined);

    useEffect(() => {
        if (term === "") {
            setResultList(null);
            return;
        }

        setResultList(undefined);

        if (type === "users") {
            UserProfileService.getUserProfileList({
                name__icontains: term
            }).then((data) => {
                setResultList(data.user_profile_list);
            })
        } else {
            GameProfileService.getGameProfileList({
                name__icontains: term
            }).then((data) => {
                setResultList(data.game_profile_list);
            })
        }
    }, [term, type])

    return (
        <SearchView
            type={type}
            resultList={resultList}
        />
    )
}


export default SearchViewContainer;