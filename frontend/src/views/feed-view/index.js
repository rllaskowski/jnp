import React, {useState, useEffect} from "react";
import FeedView from "./feed";
import GameReviewsService from "../../services/game-reviews";
import UserRelationsService from "../../services/user-relations";
import {isLoggedInSelector} from "../../store/auth";
import {useSelector} from "react-redux";
import {Navigate} from "react-router-dom";
import routes from "../../constant/routes";



const FeedViewContainer = () => {
    const isLoggedIn = useSelector(isLoggedInSelector);
    const [gameReviewList, setGameReviewList] = useState([]);

    useEffect(() => {
        if (!isLoggedIn) {
            return;
        }

        UserRelationsService.getFollowed()
            .then((data) => {
                const games_ids = data.followed_games_ids;
                const users_ids = data.followed_users_ids;
                if (games_ids.length === 0 && users_ids.length === 0) {
                    setGameReviewList([]);
                    return;
                }

                return GameReviewsService.getGameReviewList({
                    games_ids,
                    users_ids
                }, null, 0);
            }).then((data) => {
                setGameReviewList(data.review_list);
            })

    }, [isLoggedIn]);


    if (!isLoggedIn) {
        return (
            <Navigate to={routes.LOGIN}/>
        );
    }

    return (
        <FeedView gameReviewList={gameReviewList}/>
    )
}


export default FeedViewContainer;