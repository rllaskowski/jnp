import React from "react";
import Footer from "../../components/footer";
import NavigationDrawer from "../../components/navigation-drawer";
import GameReviewList from "../../components/game-review-list";
import ContentWrapper from "../../components/content-wrapper";
import Spinner from "../../components/spinner";


const FeedView = ({gameReviewList}) => {
    return (
        <NavigationDrawer>
            <ContentWrapper>
                {gameReviewList === undefined?
                    <Spinner/> :
                 gameReviewList.length === 0?
                     <h2 className={"text-center font-bold text-2xl mt-10"}>
                         Ups! Nie ma tu nic do pokazania...
                     </h2> :
                    <GameReviewList gameReviewList={gameReviewList} showGameProfileLink={true}/>
                }
            </ContentWrapper>
            <Footer/>
        </NavigationDrawer>
    );
}

export default FeedView;