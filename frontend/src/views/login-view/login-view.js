import React from "react";
import Footer from "../../components/footer";
import NavigationDrawer from "../../components/navigation-drawer";
import LoginForm from "../../components/login-form";
import ContentWrapper from "../../components/content-wrapper";


const LoginView = () => {
    return (
        <NavigationDrawer>
            <ContentWrapper>
                <LoginForm/>
            </ContentWrapper>
            <Footer/>
        </NavigationDrawer>
    )
}


export default LoginView;