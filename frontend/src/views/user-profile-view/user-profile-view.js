import React from "react";
import Footer from "../../components/footer";
import NavigationDrawer from "../../components/navigation-drawer";
import ContentWrapper from "../../components/content-wrapper";
import Spinner from "../../components/spinner";
import GameReviewList from "../../components/game-review-list";


const UserInfoCard = ({userProfile, onFollowToggle, isFollowed, userImage}) => {
    const onNoImageError = (evt) => {
        evt.target.src = "/img/default-profile-picture.jpg";
    }

    return (
        <div className="card text-center shadow-lg max-w-screen-md mx-auto">
            <figure className="px-10 pt-10">
                <div className="avatar ">
                    <div className={"w-40 h-40 rounded-full mx-auto border"}>
                        <img src={userImage} alt={"User profile picture"} onError={onNoImageError}/>
                    </div>
                </div>
            </figure>
            <div className="card-body">
                <h2 className="card-title">{`${userProfile.name} ${userProfile.surname}`}</h2>
                <p>{userProfile.description}</p>
                <div className="justify-center card-actions">
                    {isFollowed !== undefined &&
                        <button
                            className={`btn ${!isFollowed? "btn-outline" : ""} btn-accent`}
                            onClick={onFollowToggle}
                        >
                            {isFollowed? "Nie obserwuj" : "Obserwuj"}
                        </button>
                    }
                </div>
            </div>
        </div>
    );
}


const UserProfileView = ({userProfile, reviewList, onFollowToggle, isFollowed, userImage}) => {
    return (
        <NavigationDrawer>
            <ContentWrapper>
                {userProfile === undefined?
                    <Spinner/> :
                    <UserInfoCard
                        userImage={userImage}
                        userProfile={userProfile}
                        onFollowToggle={onFollowToggle}
                        isFollowed={isFollowed}
                    />
                }
                {reviewList === undefined?
                    <Spinner/> :
                reviewList.length === 0?
                    <h2 className={"text-xl font-bold text-center my-5"}>
                        Ten użytkownik nie napisał jeszcze żadnych recenzji gier
                    </h2> :
                    <GameReviewList gameReviewList={reviewList} showGameProfileLink={true}/>
                }
            </ContentWrapper>
            <Footer/>
        </NavigationDrawer>
    );
}


export default UserProfileView;