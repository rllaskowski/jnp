import React, {useCallback, useEffect, useState} from "react";
import UserProfileService from "../../services/user-profile";
import UserProfileView from "./user-profile-view";
import GameReviewsService from "../../services/game-reviews";
import { useParams } from "react-router-dom";
import UserRelationsService from "../../services/user-relations";
import {useSelector} from "react-redux";
import {isLoggedInSelector} from "../../store/auth";


const UserProfileViewContainer = () => {
    const { userId } = useParams();
    const [userProfile, setUserProfile] = useState(undefined);
    const [reviewList, setReviewList] = useState(undefined);
    const [isFollowed, setIsFollowed] = useState(undefined);
    const isLoggedIn = useSelector(isLoggedInSelector);
    const userImage = UserProfileService.getUserImage(userId);

    useEffect(() => {
        setUserProfile(undefined);
        setReviewList(undefined);
        setIsFollowed(undefined);

        UserProfileService.getUserProfile(userId)
            .then((userProfile) => setUserProfile(userProfile));

        GameReviewsService.getGameReviewList({
            user_id: userId
        }, -1, 0).then((data) => {
            setReviewList(data.review_list);
            });

    }, [userId]);

    useEffect(() => {
        if (isLoggedIn) {
            UserRelationsService.isFollowedUser(userId)
                .then((data) => setIsFollowed(data.is_followed))
        }
    }, [userId, isLoggedIn])

    const onFollowToggle = useCallback(() => {
        if (!isLoggedIn || isFollowed === undefined) {
            return;
        }

        setIsFollowed(isFollowed => !isFollowed);
        if (isFollowed) {
            UserRelationsService.unfollowUser(userId)
        } else {
            UserRelationsService.followUser(userId);
        }
    }, [isFollowed, userId, isLoggedIn]);


    return (
        <UserProfileView
            userImage={userImage}
            userProfile={userProfile}
            isFollowed={isFollowed}
            onFollowToggle={onFollowToggle}
            reviewList={reviewList}
        />
    )
}


export default UserProfileViewContainer;