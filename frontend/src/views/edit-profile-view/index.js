import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {isLoggedInSelector} from "../../store/auth";
import {Navigate} from "react-router-dom";
import routes from "../../constant/routes";
import EditProfileView from "./edit-profile-view";
import UserProfileService from "../../services/user-profile";


const EditProfileViewContainer = () => {
    const isLoggedIn = useSelector(isLoggedInSelector);
    const [userProfile, setUserProfile] = useState();
    const [imageUrl, setImageUrl] = useState();

    useEffect(() => {
        if (!isLoggedIn) { return; }
        setUserProfile(undefined);

        UserProfileService.getAuthenticatedUserProfile()
            .then((data) => {
                setUserProfile(data);
                setImageUrl(UserProfileService.getUserImage(data.id));
            });
    }, [isLoggedIn])

    const onEdit = (data) => {
        UserProfileService.editProfile(data);
    }

    const onImageUpload = (file) => {
        UserProfileService.uploadImage(file)
            .then(() => {
                setImageUrl(url => `${url}?`);
            })
    }

    if (!isLoggedIn) {
        return (
            <Navigate to={routes.LOGIN}/>
        );
    }
    return (
        <EditProfileView
            userProfile={userProfile}
            onEdit={onEdit}
            imageUrl={imageUrl}
            onImageUpload={onImageUpload}
        />
    );
}


export default EditProfileViewContainer;