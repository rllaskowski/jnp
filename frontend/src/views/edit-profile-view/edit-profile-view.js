import React, {useRef, useState} from "react";
import Footer from "../../components/footer";
import NavigationDrawer from "../../components/navigation-drawer";
import ContentWrapper from "../../components/content-wrapper";
import Spinner from "../../components/spinner";
import UploadImageModal from "../../components/upload-image-modal";

const onNoImageError = (evt) => {
    evt.target.src = "/img/default-profile-picture.jpg";
}


const UserEditProfileCard = ({userProfile, onEdit, onImageUpload, imageUrl}) => {
    const descriptionInputRef = useRef();

    const onSubmit = () => {
        onEdit({
            description: descriptionInputRef.current.value
        })
    }

    return (
        <div className="card text-center shadow-lg max-w-screen-md mx-auto">
            <figure className="px-10 pt-10">
                <div className="avatar ">
                    <div className={"w-40 h-40 rounded-full mx-auto border"}>
                        <img src={imageUrl} alt={"User profile picture"} onError={onNoImageError}/>
                    </div>
                </div>
            </figure>
            <UploadImageModal onImageUpload={onImageUpload} buttonTitle={"Zmień zdjęcie"}/>
            <div className="card-body">
                <h2 className="card-title">{`${userProfile.name} ${userProfile.surname}`}</h2>

                <div className="p-10 card bg-base-200">
                    <div className="form-control">
                        <textarea
                            ref={descriptionInputRef} maxLength={500}
                            className="textarea h-24 textarea-ghost" placeholder={"Powiedz coś o sobie"}
                        >
                            {userProfile.description}
                        </textarea>
                    </div>
                </div>

            </div>

            <button className={"w-full btn btn-primary"} onClick={onSubmit}>
                Zapisz zmiany
            </button>
        </div>
    );
}

const EditProfileView = ({userProfile, onEdit, onImageUpload, imageUrl}) => {
    return (
        <NavigationDrawer>
            <ContentWrapper>
                {userProfile === undefined?
                    <Spinner/> :
                    <UserEditProfileCard
                        userProfile={userProfile}
                        onEdit={onEdit}
                        imageUrl={imageUrl}
                        onImageUpload={onImageUpload}
                    />
                }
            </ContentWrapper>
            <Footer/>
        </NavigationDrawer>
    );
}

export default EditProfileView;