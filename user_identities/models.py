from config import db


class UserIdentity(db.Document):
    email = db.StringField(required=True)  # TODO maybe add validation
    password = db.StringField(required=True)  # TODO hash

    def __repr__(self):
        return f'{self.email}'
