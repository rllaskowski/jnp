from flask import abort, request, make_response
from marshmallow import Schema, fields, ValidationError, validate
from werkzeug.security import generate_password_hash, check_password_hash
from flask_jwt_extended import create_access_token, create_refresh_token, JWTManager, jwt_required
from config import app, SERVICE_NAME
from models import UserIdentity


def load_json(schema, partial=False):
    json_data = request.get_json()
    if not json_data:
        abort(make_response({'errors': ['No input data provided.']}, 400))
    try:
        data = schema.load(json_data, partial=partial)
    except ValidationError as err:
        abort(make_response(err.messages, 422))
    return data


class AuthenticationSchema(Schema):
    email = fields.String(required=True, load_only=True, validate=validate.Email())
    password = fields.String(required=True, load_only=True)


auth_schema = AuthenticationSchema()

jwt = JWTManager(app)


@jwt.user_identity_loader
def user_identity_lookup(user):
    return str(user.id)


@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    return jwt_data["sub"]


@app.route('/register', methods=['POST'])
def register():
    data = load_json(auth_schema)
    if UserIdentity.objects(email=data['email']).first():
        abort(make_response({'email': ['Użytkownik o podanym adresie już istnieje']}, 400))

    data['password'] = generate_password_hash(data['password'])
    user = UserIdentity(**data)
    user.save()

    return {'id': str(user.pk)}


@app.route('/validate_token', methods=['POST'])
@jwt_required()
def validate_token():
    return 'OK'


@app.route('/login', methods=['POST'])
def login():
    data = load_json(auth_schema)
    user = UserIdentity.objects(email=data['email']).first()

    if not user or not check_password_hash(user.password, data['password']):
        abort(make_response({'error': ['Niepoprawny email lub hasło']}, 401))

    response_data = {
        'access_token': create_access_token(user),
        'refresh_token': create_refresh_token(user)
    }

    return response_data


@app.route('/')
def healthcheck():
    return f'Service {SERVICE_NAME}: OK'
