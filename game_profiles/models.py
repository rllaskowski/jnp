from datetime import datetime
from config import db


class GameProfile(db.Document):
    name = db.StringField(required=True)
    image = db.ImageField(thumbnail_size=(48, 48, True))  # TODO add constraint png or jpeg
    description = db.StringField()
    release_date = db.DateField()
    genre = db.StringField()
    tags = db.ListField(field=db.StringField())
    creation_time = db.DateTimeField(default=datetime.now)
    modification_time = db.DateTimeField(default=datetime.now)

    def __repr__(self):
        return f'{self.name} {self.creation_time} {self.modification_time}'

    def save(self, *args, **kwargs):
        self.modification_time = datetime.now()
        return super().save(*args, **kwargs)

    def modify(self, **update):
        return super().modify(modification_time=datetime.now(), **update)
