import views
from config import app

game_profiles_view = views.GameProfileAPIVIew.as_view('game_profiles')
game_profiles_list_view = views.GameProfileListAPIVIew.as_view('game_profiles_list')
game_profiles_image_view = views.GameProfileImageAPIVIew.as_view('image')


app.add_url_rule('/game_profiles/list', view_func=game_profiles_list_view, methods=['GET'])
app.add_url_rule('/game_profiles', view_func=game_profiles_view, methods=['POST'])
app.add_url_rule('/game_profiles/<game_id>', view_func=game_profiles_view,
                 methods=['GET', 'PATCH', 'DELETE'])
app.add_url_rule('/game_profiles/<game_id>/image', view_func=game_profiles_image_view,
                 methods=['GET', 'POST'])

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)
