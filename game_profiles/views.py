from flask import request, abort, make_response, Response
from flask.views import MethodView
from marshmallow import Schema, fields, ValidationError

from config import app, cache, SERVICE_NAME
from models import GameProfile


def validate_mongo_id(x):
    if len(x) != 12 and len(x) != 24:
        raise ValidationError('Incorrect mongo id.')


def abort_on_incorrect_mongo_id(mongo_id):
    try:
        validate_mongo_id(mongo_id)
    except ValidationError as err:
        abort(make_response({'id': err.messages}, 422))


def load_json(schema, partial=False):
    json_data = request.get_json()
    if not json_data:
        abort(make_response({'errors': ['No input data provided.']}, 400))
    try:
        data = schema.load(json_data, partial=partial)
    except ValidationError as err:
        abort(make_response(err.messages, 422))
    return data


class GameProfileSchema(Schema):
    id = fields.String(dump_only=True)
    name = fields.String(required=True)
    description = fields.String()
    release_date = fields.Date()
    genre = fields.String()
    tags = fields.List(fields.String())


class GameProfileListAPIVIew(MethodView):
    def __init__(self):
        self.schema = GameProfileSchema()

    def get(self):
        query_params = request.args.to_dict()

        count = query_params.get('count')
        offset = query_params.get('offset', 0)

        if count:
            game_profile_list = GameProfile.objects(**request.args)[offset:count]
        else:
            game_profile_list = GameProfile.objects(**request.args)[offset:]

        return {
            'game_profile_list': self.schema.dump(game_profile_list, many=True)
        }


class GameProfileAPIVIew(MethodView):
    def __init__(self):
        self.schema = GameProfileSchema()

    @cache.cached()
    def get(self, game_id):
        abort_on_incorrect_mongo_id(game_id)
        game = GameProfile.objects(pk=game_id)\
            .first_or_404(message='Resource not found.')
        return self.schema.dump(game)

    def post(self):
        data = load_json(self.schema)
        game = GameProfile(**data)
        game.save()
        return self.schema.dump(game), 201

    def patch(self, game_id):
        abort_on_incorrect_mongo_id(game_id)
        game = GameProfile.objects(pk=game_id) \
            .first_or_404(message='Resource not found.')
        data = load_json(self.schema, partial=True)
        game.modify(**data)
        game.reload()
        return self.schema.dump(game)

    def delete(self, game_id):
        abort_on_incorrect_mongo_id(game_id)
        game = GameProfile.objects(pk=game_id) \
            .first_or_404(message='Resource not found.')
        if game.image:
            game.image.delete()
        game.delete()
        return '', 204


class GameProfileImageAPIVIew(MethodView):
    @cache.cached()
    def get(self, game_id):
        abort_on_incorrect_mongo_id(game_id)
        game = GameProfile.objects(pk=game_id)\
            .first_or_404(message='Resource not found.')
        if not game.image:
            abort(make_response({'errors': ['Image not found.']}, 400))
        return Response(response=game.image.read(), content_type=game.image.content_type)

    def post(self, game_id):
        abort_on_incorrect_mongo_id(game_id)
        if 'image' in request.files:
            game = GameProfile.objects(pk=game_id) \
                .first_or_404(message='Resource not found.')
            image = request.files['image']
            if not game.image:
                game.image.put(image, content_type=image.content_type)
            else:
                game.image.replace(image, content_type=image.content_type)
            game.save()
        else:
            abort(make_response({'errors': ['No image provided.']}, 400))
        return '', 204


@app.route('/')
def healthcheck():
    return f'Service {SERVICE_NAME}: OK'
